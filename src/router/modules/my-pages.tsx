import { lazy } from 'react'
import lazyLoad from '@/router/utils/lazyLoad'
import { FileImageOutlined, FilePdfOutlined, FolderOutlined, NodeIndexOutlined, PrinterOutlined, QrcodeOutlined } from '@ant-design/icons'
import { Stamp } from '@carbon/icons-react'

export default [{
  path: '/my-pages',
  key: '/my-pages',
  label: '页面',
  title: '页面',
  icon: <FolderOutlined />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  children: [
    {
      label: '打印',
      title: '打印',
      icon: <PrinterOutlined />,
      path: '/my-pages/print',
      key: '/my-pages/print',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "print" */ '@/pages/my-pages/print'))),
    },
    {
      label: 'PDF预览',
      title: 'PDF预览',
      icon: <FilePdfOutlined />,
      path: '/my-pages/pdf',
      key: '/my-pages/pdf',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "pdf" */ '@/pages/my-pages/pdf'))),
    },
    {
      label: '图标',
      title: '图标',
      icon: <NodeIndexOutlined />,
      path: '/my-pages/icon',
      key: '/my-pages/icon',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "icon" */ '@/pages/my-pages/icon'))),
    },
    {
      label: '二维码',
      title: '二维码',
      icon: <QrcodeOutlined />,
      path: '/my-pages/qrcode',
      key: '/my-pages/qrcode',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "qrcode" */ '@/pages/my-pages/qrcode'))),
    },
    {
      label: '图片预览',
      title: '图片预览',
      icon: <FileImageOutlined />,
      path: '/my-pages/viewer',
      key: '/my-pages/viewer',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "viewer" */ '@/pages/my-pages/viewer'))),
    },
    {
      label: '水印',
      title: '水印',
      icon: <Stamp />,
      path: '/my-pages/watermark',
      key: '/my-pages/watermark',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "watermark" */ '@/pages/my-pages/watermark'))),
    },
    // {
    //   label: '天气预报',
    //   title: '天气预报',
    //   icon: <RainScattered />,
    //   path: '/my-pages/weather',
    //   key: '/my-pages/weather',
    //   element: lazyLoad(lazy(() => import(/* webpackChunkName: "weather" */ '@/pages/my-pages/weather'))),
    // },
  ],
}]
