import { lazy } from 'react'
import lazyLoad from '@/router/utils/lazyLoad'
import { FileOutlined, TableOutlined } from '@ant-design/icons'

export default [{
  path: '/table-pages',
  key: '/table-pages',
  label: '表格',
  title: '表格',
  icon: <TableOutlined />,
  element: lazyLoad(lazy(() => import('@/layouts/index'))),
  children: [
    {
      label: '查询表格',
      title: '查询表格',
      icon: <FileOutlined />,
      path: '/table-pages/table',
      key: '/table-pages/table',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "basic-table" */ '@/pages/table-pages/table'))),
    },
    {
      label: '标准表格',
      title: '标准表格',
      icon: <FileOutlined />,
      path: '/table-pages/standard',
      key: '/table-pages/standard',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "standard-table" */ '@/pages/table-pages/standard'))),
    },
    {
      label: '卡片列表',
      title: '卡片列表',
      icon: <FileOutlined />,
      path: '/table-pages/card-list',
      key: '/table-pages/card-list',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "card-list-table" */ '@/pages/table-pages/card-list'))),
    },
    {
      label: '懒加载列表',
      title: '懒加载列表',
      icon: <FileOutlined />,
      path: '/table-pages/lazy-table',
      key: '/table-pages/lazy-table',
      element: lazyLoad(lazy(() => import(/* webpackChunkName: "lazy-table" */ '@/pages/table-pages/lazy-table'))),
    },
  ],
}]
