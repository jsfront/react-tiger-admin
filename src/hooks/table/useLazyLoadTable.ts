import { useStore } from '@/store'
import { debounce } from 'lodash'
import { useCallback, useState } from 'react'

export const useTableLazyLoad = () => {
  const [page, setPage] = useState(2)
  const [loading, setLoading] = useState(false)
  const { tableStore } = useStore()

  const loadMore = useCallback(
    debounce(async () => {
      if (loading) return
      setLoading(true)
      try {
        await tableStore.fetchData({ current: page })
        setPage((prev) => prev + 1)
      } finally {
        setLoading(false)
      }
    }, 300),
    [page, loading],
  )

  return { page, loading, loadMore }
}
