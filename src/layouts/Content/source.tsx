import React, { FC } from 'react'
import { useLocation } from 'react-router-dom'
import { Gitee } from '@/common/constants'
import { Space } from 'antd'

const Source: FC = () => {
  const { pathname } = useLocation()
  const url = `${Gitee}/blob/master/src/pages${pathname}/index.tsx`

  return (
    <div className="app-source">
      <a href={url} target="_blank" rel="noopener noreferrer">
        <Space>
          &lt;/&gt;
          <span>查看源码</span>
        </Space>
      </a>
    </div>
  )
}

export default Source
