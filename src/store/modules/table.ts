import { action, makeObservable, observable } from 'mobx'
import { getUserList } from '@/data'

class Table {
  constructor() {
    makeObservable(this)
    this.fetchData({})
  }

  @observable total = 0

  @observable list = []

  @action setList = (list: any) => {
    this.list = list
  }

  @action fetchData = async ({ current = 1, pageSize = 10 }) => {
    const res = await getUserList({ current, pageSize }, {})
    this.total = res?.total
    this.setList(current === 1 ? res?.list : [...this.list, ...res?.list])
  }
}

export default new Table()
