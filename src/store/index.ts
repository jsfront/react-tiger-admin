import { createContext, useContext } from 'react'
import appStore from './modules/app'
import userStore from './modules/user'
import tagStore from './modules/tag'
import tableStore from './modules/table'

const store = {
  appStore,
  userStore,
  tagStore,
  tableStore,
}

const StoreContext = createContext(store)

export const useStore = () => useContext(StoreContext)

export default store
