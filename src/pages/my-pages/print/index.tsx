import { FC, useRef } from 'react'
import { Button, Card, Space, Table, Tag } from 'antd'
import View from '@/components/View'
import ReactToPrint from 'react-to-print'
import type { ColumnsType } from 'antd/es/table'

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
}

const columns: ColumnsType<DataType> = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: (_, { tags }) => (
      <>
        {tags.map((tag) => {
          let color = tag.length > 5 ? 'geekblue' : 'green'
          if (tag === 'loser') {
            color = 'volcano'
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          )
        })}
      </>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (_, record) => (
      <Space size="middle">
        {record.name}
        Delete
      </Space>
    ),
  },
]

const data: DataType[] = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
]

const Print: FC = () => {
  const componentRef = useRef<HTMLDivElement>(null)

  return (
    <View className="app-page-print">
      <Card type="inner" title="打印">
        <div className="mb-5">
          <ReactToPrint
            // pageStyle={printTableStyle}
            trigger={() => <Button type="primary">打印表格</Button>}
            content={() => componentRef.current}
          />
        </div>
        <div ref={componentRef}>
          <Table bordered columns={columns} dataSource={data} />
        </div>
      </Card>
    </View>
  )
}

export default Print
