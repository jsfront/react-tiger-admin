import { Card, Col, Row } from 'antd'
import Weather from 'react-tencent-weather'

const MyWeather = () => {
  const city = window.returnCitySN?.cname

  return (
    <div className="app-page-weather">
      <Row gutter={[24, 24]}>
        <Col span={6}>
          <Card bordered={false} title="默认显示">
            <Weather province={city} city={city} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="不显示逐小时预报">
            <Weather province="上海" city="上海" showHours={false} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="不显示生活指数">
            <Weather province="上海" city="上海" showLiving={false} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="不显示7日天气预报">
            <Weather province={city} city={city} showDays={false} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="不显示明日预报">
            <Weather province="上海" city="上海" showTomorrow={false} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="不显示主区域" className="app-weather-no-main">
            <Weather province="上海" city="上海" />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="7日天气预报" className="app-weather-no-main">
            <Weather province="上海" city="上海" showHours={false} showLiving={false} showTomorrow={false} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} title="最小区域" className="app-weather-no-main min">
            <Weather province="上海" city="上海" showDays={false} showHours={false} showLiving={false} />
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default MyWeather
