import React from 'react'
import { Card, Button, Result } from 'antd'
import View from '@/components/View'

const StandardPages: React.FC = () => {
  return (
    <View>
      <Card title="操作成功">
        <Result
          status="success"
          title="恭喜您!您已成功购买云服务器ECS"
          subTitle="订单号:2017182818828182881云服务器配置需要1-5分钟，请稍候"
          extra={[
            <Button type="primary" key="console">
              Go Console
            </Button>,
            <Button key="buy">Buy Again</Button>,
          ]}
        />
      </Card>
    </View>
  )
}
export default StandardPages
