// import {
//   fetchUserList,
// } from '@/api/user'

interface Result {
  total: number;
  list: any;
}

// 获取实验资源列表
export const getUserList = async ({ current, pageSize }: any, formData?: any): Promise<Result> => {
  let query = `page=${current}&size=${pageSize}`
  Object.entries(formData).forEach(([key, value]) => {
    if (value) {
      query += `&${key}=${value}`
    }
  })
  return fetch(`https://randomuser.me/api?results=10&${query}`)
    .then((res) => res.json())
    .then((res) => ({
      total: res.info.results,
      list: res.results,
    }))

  // const p = { params: { ...formData, page: current, pagesize: pageSize } }
  // Object.assign(p, { type: 'get' })

  // const result = await fetchUserList(p)

  // return {
  //   total: 200,
  //   list: result,
  // }
}
